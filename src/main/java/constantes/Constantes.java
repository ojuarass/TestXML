package constantes;

import java.util.Map;

public class Constantes {
	public static final String XML_LABEL = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	public static final String DOCUMENT_INIT = "<Document>";
	public static final String DOCUMENT_END = "</Document>";
	public static final String CstmrCdtTrfInitn_INIT = "<CstmrCdtTrfInitn>";
	public static final String CstmrCdtTrfInitn_END = "</CstmrCdtTrfInitn>";

	public static final String GrpHdr_INIT = "<GrpHdr>";
	public static final String GrpHdr_END = "</GrpHdr>";

	public static final String PmtInf_INIT = "<PmtInf>";
	public static final String PmtInf_END = "</PmtInf>";

	public static final String MsgId_INIT = "<MsgId>";
	public static final String MsgId_END = "</MsgId>";

	public static final String CreDtTm_INIT = "<CreDtTm>";
	public static final String CreDtTm_END = "</CreDtTm>";

	public static final String NbOfTxs_INIT = "<NbOfTxs>";
	public static final String NbOfTxs_END = "</NbOfTxs>";

	public static final String CtrlSum_INIT = "<CtrlSum>";
	public static final String CtrlSum_END = "</CtrlSum>";

	public static final String InitgPty_INIT = "<InitgPty>";
	public static final String InitgPty_END = "</InitgPty>";

	public static final String Nm_INIT = "<Nm>";
	public static final String Nm_END = "</Nm>";

	public static final String PstlAdr_INIT = "<PstlAdr>";
	public static final String PstlAdr_END = "</PstlAdr>";

	public static final String StrtNm_INIT = "<StrtNm>";
	public static final String StrtNm_END = "</StrtNm>";

	public static final String BldgNb_INIT = "<BldgNb>";
	public static final String BldgNb_END = "</BldgNb>";

	public static final String PstCd_INIT = "<PstCd>";
	public static final String PstCd_END = "</PstCd>";

	public static final String TwnNm_INIT = "<TwnNm>";
	public static final String TwnNm_END = "</TwnNm>";

	public static final String Ctry_INIT = "<Ctry>";
	public static final String Ctry_END = "</Ctry>";

	public static final String PmtInfId_INIT = "<PmtInfId>";
	public static final String PmtInfId_END = "</PmtInfId>";

	public static final String PmtMtd_INIT = "<PmtMtd>";
	public static final String PmtMtd_END = "</PmtMtd>";

	public static final String PmtTpInf_INIT = "<PmtTpInf>";
	public static final String PmtTpInf_END = "</PmtTpInf>";

	public static final String SvcLvl_INIT = "<SvcLvl>";
	public static final String SvcLvl_END = "</SvcLvl>";

	public static final String Cd_INIT = "<Cd>";
	public static final String Cd_END = "</Cd>";

	public static final String LclInstrm_INIT = "<LclInstrm>";
	public static final String LclInstrm_END = "</LclInstrm>";

	public static final String Prtry_INIT = "<Prtry>";
	public static final String Prtry_END = "</Prtry>";

	public static final String ReqdExctnDt_INIT = "<ReqdExctnDt>";
	public static final String ReqdExctnDt_END = "</ReqdExctnDt>";

	public static final String Dbtr_INIT = "<Dbtr>";
	public static final String Dbtr_END = "</Dbtr>";

	public static final String DbtrAcct_INIT = "<DbtrAcct>";
	public static final String DbtrAcct_END = "</DbtrAcct>";

	public static final String Id_INIT = "<Id>";
	public static final String Id_END = "</Id>";

	public static final String Othr_INIT = "<Othr>";
	public static final String Othr_END = "</Othr>";

	public static final String DbtrAgt_INIT = "<DbtrAgt>";
	public static final String DbtrAgt_END = "</DbtrAgt>";
	public static final String FinInstnId_INIT = "<FinInstnId>";
	public static final String FinInstnId_END = "</FinInstnId>";
	public static final String BIC_INIT = "<BIC>";
	public static final String BIC_END = "</BIC>";
	public static final String BrnchId_INIT = "<BrnchId>";
	public static final String BrnchId_END = "</BrnchId>";
	public static final String CdtTrfTxInf_INIT = "<CdtTrfTxInf>";
	public static final String CdtTrfTxInf_END = "</CdtTrfTxInf>";
	public static final String PmtId_INIT = "<PmtId>";
	public static final String PmtId_END = "</PmtId>";
	public static final String EndToEndId_INIT = "<EndToEndId>";
	public static final String EndToEndId_END = "</EndToEndId>";
	public static final String Amt_INIT = "<Amt>";
	public static final String Amt_END = "</Amt>";
	public static final String InstdAmt_INIT = "<InstdAmt>";
	public static final String InstdAmt_END = "</InstdAmt>";
	public static final String Cdtr_INIT = "<Cdtr>";
	public static final String Cdtr_END = "</Cdtr>";
	public static final String CdtrAcct_INIT = "<CdtrAcct>";
	public static final String CdtrAcct_END = "</CdtrAcct>";
	public static final String Tp_INIT = "<Tp>";
	public static final String Tp_END = "</Tp>";
	public static final String RmtInf_INIT = "<RmtInf>";
	public static final String RmtInf_END = "</RmtInf>";
	public static final String Ustrd_INIT = "<Ustrd>";
	public static final String Ustrd_END = "</Ustrd>";

	public static final String CONS_TIPO_REGISTRO = "1";
	public static final String CONS_NOMBRE_EMPRESA = "Empresa Demo                        ";
	public static final String CONS_NAT_ARCHIVO = "15";
	public static final String CONS_VERSION_LAYOUT = "D";
	public static final String CONS_TIPO_CARGO = "01";

	public static final String[] ARRAY_EMPRESA = { "América Móvil", "Walmart de México",
			"Comisión Federal de Electricidad", "General Motors de México", "Grupo Bimbo",
			"Grupo Financiero BBVA Bancomer", "Nissan Mexicana", "Coca-Cola FEMSA", "Ford de México",
			"Volkswagen de México", "Grupo Financiero Banamex", "Grupo Coppel", "Samsung Mexico", "Grupo Salinas",
			"Grupo Financiero Santander México", "Grupo Televisa", "Toyota Motor Sales de México",
			"LG Electronics México", "BMW Group México", "Grupo Financiero HSBC" };

	public static final String[] ARRAY_BENEFICIARIO = { "ARTURO TREJO NAVA", "LORENA LARA SANCHEZ",
			"MANUEL TREVI ESCUDERO", "GABRIEL ROSENZWEIG PICHARDO", "PILAR GOMEZ OLIVER",
			"JOSE MONTIEL HERNANDEZ", "RICARDO PINEDA ALBARRAN", "MARIANO MARTINEZ GOMEZ",
			"ENRIQUE ESCAMILLA NUNEZ", "CARMEN RAMIREZ MORALES",
			"ROBERTO MARTINEZ MARTINEZ", "GERMAN MURGUIA MIER", "VICENTE SANCHEZ VENTURA",
			"PATRICIA LORENZO VALDES", "ERNESTO TORICES MORELOS", "JESUS GUTIERREZ MONROY",
			"EMILIA HERNANDEZ MARTINEZ", "LORENA GONZALEZ CASILLAS", "ROCIO MARQUEZ ESQUIVEL",
			"JUAN MIRAMONTES ACOSTA", "RAUL VALDES AGUILAR", "SERGIO GONZALEZ GALVEZ",
			"ROSA BETANCOURT MORENO", "FEDERICO SALAS LOTFE", "JAIME NUALART SANCHEZ",
			"ELSA BORJA SANCHEZ", "LUISA SANTOS DAMIAN", "CARLOS GARCIA ZEPEDA",
			"CAROLINA ZARAGOZA FLORES" };

	public static final String[] ARRAY_REFERENCIA = { "INBA", "IPN", "UAM", "UNAM", "UPN", "INBA", "CUGS", "CUSA",
			"ESEF", "CIESAS", "COLMEX", "EDN", "FLACSO", "INC", "INAH", "INR" };
}
