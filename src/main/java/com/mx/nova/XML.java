package com.mx.nova;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class XML extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7217808256468070098L;
	JLabel lblCantidad;
	JSpinner spinner;
	JFileChooser fileSelect;

	JButton btnGenerar = new JButton("Generar XML");
	GridLayout gridLayout = new GridLayout(0, 2);

	public XML(String name) {
		super(name);
		setResizable(false);
	}

	public void initGaps() {
		lblCantidad = new JLabel("Número de elementos: ");
		SpinnerModel sm = new SpinnerNumberModel(1, 1, 100_000, 1);
		spinner = new JSpinner(sm);
		fileSelect = new JFileChooser();
	}

	public void addComponentsToPane(final Container pane) {
		initGaps();
		final JPanel compsToExperiment = new JPanel();
		compsToExperiment.setLayout(gridLayout);
		JPanel controls = new JPanel();
		controls.setLayout(new GridLayout(2, 2));

		compsToExperiment.add(lblCantidad);
		compsToExperiment.add(spinner);

		gridLayout.setHgap(15);
		gridLayout.setVgap(15);
		gridLayout.layoutContainer(compsToExperiment);

		controls.add(btnGenerar);

		btnGenerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int quantity = (Integer) spinner.getValue();
				String filePath = "";

				JFileChooser fileChooser = new JFileChooser();

				int rVal = fileChooser.showOpenDialog(XML.this);
				if (rVal == JFileChooser.APPROVE_OPTION) {
					filePath = fileChooser.getCurrentDirectory().toString().concat("/")
							.concat(fileChooser.getSelectedFile().getName());
					if (!filePath.contains(".xml")) {
						filePath = filePath.split("\\.")[0].concat(".xml");
					}

					if (CreateXML.generateXML(filePath, quantity)) {
						JOptionPane.showMessageDialog(null, "Se guardo exitosamente el archivo", "Resultado",
								JOptionPane.INFORMATION_MESSAGE);
					}

				}
			}
		});

		pane.add(compsToExperiment, BorderLayout.NORTH);
		pane.add(new JSeparator(), BorderLayout.CENTER);
		pane.add(controls, BorderLayout.SOUTH);
	}

	private static void createAndShowGUI() {

		XML frame = new XML("Generación de XML");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addComponentsToPane(frame.getContentPane());
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		UIManager.put("swing.boldMetal", Boolean.FALSE);

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}