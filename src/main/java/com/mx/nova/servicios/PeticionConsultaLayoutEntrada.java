package com.mx.nova.servicios;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mx.nova.constantes.Constantes;
import com.mx.nova.exception.GenericException;
import com.mx.nova.modelo.Definition;
import com.mx.nova.modelo.LayoutsInfoGeneralArchivo;
import com.mx.nova.repository.ConsultaDatosLayouts;

public class PeticionConsultaLayoutEntrada implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8270539232916276388L;
	private ConsultaDatosLayouts consultaDatosLayouts = new ConsultaDatosLayouts();

	public LayoutsInfoGeneralArchivo consultaLayoutArchivoGeneral() throws GenericException {
		LayoutsInfoGeneralArchivo layoutsInfoGeneralArchivo = null;

		try {
			Map<String, Object> datosBusquedaConsulta = new HashMap<>();
			datosBusquedaConsulta.put(Constantes.BANCO, Constantes.CITIBANAMEX);

			List<String> excluirDatos = new ArrayList<>();
			excluirDatos.add(Constantes.ID);

			layoutsInfoGeneralArchivo = consultaDatosLayouts.consultaDatosGeralesArchivo(datosBusquedaConsulta,
					excluirDatos);

		} catch (Exception e) {
			throw new GenericException(Constantes.LOG_ERROR_EN_CONSULTAR_LAYOUTS, e);
		}

		return layoutsInfoGeneralArchivo;
	}

	public Definition consultaDefinicionEntrada() throws GenericException {

		Definition definition = null;

		try {
			Map<String, Object> datosBusquedaConsulta = new HashMap<>();
			datosBusquedaConsulta.put(Constantes.BANCO, Constantes.CITIBANAMEX);

			List<String> excluirDatos = new ArrayList<>();
			excluirDatos.add(Constantes.ID);

			definition = consultaDatosLayouts.consultaDefinicionEntrada(datosBusquedaConsulta, excluirDatos);

		} catch (Exception e) {
			throw new GenericException(Constantes.LOG_ERROR_EN_CONSULTAR_LAYOUTS, e);
		}

		return definition;
	}

}
