package com.mx.nova.repository;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.nova.constantes.Constantes;
import com.mx.nova.exception.GenericException;
import com.mx.nova.modelo.Definition;
import com.mx.nova.modelo.LayoutsInfoGeneralArchivo;
import com.mx.nova.util.Maper;

public class ConsultaDatosLayouts {

	private LecturaLayouts lecturaLayouts;

	public LayoutsInfoGeneralArchivo consultaDatosGeralesArchivo(Map<String, Object> datosBusquedaConsulta,
			List<String> excluirDatos) throws GenericException {

		LayoutsInfoGeneralArchivo layoutsInfoGeneralArchivo = null;

		try {
			for (LayoutsInfoGeneralArchivo layoutsInfoGeneralArchivoObj : lecturaLayouts.getInstance()
					.lecturaLayoutGeneral()) {
				if (layoutsInfoGeneralArchivoObj.getBank().equals(datosBusquedaConsulta.get(Constantes.BANCO))) {
					layoutsInfoGeneralArchivo = layoutsInfoGeneralArchivoObj;
				}

			}

		} catch (Exception e) {
			throw new GenericException(Constantes.LOG_ERROR_EN_CONSULTAR_LAYOUTS, e);
		}

		return layoutsInfoGeneralArchivo;
	}

	public Definition consultaDefinicionEntrada(Map<String, Object> datosBusquedaConsulta, List<String> excluirDatos)
			throws GenericException {

		ObjectMapper mapper = Maper.INSTANCE.getObjectMapper();

		Definition definicion = null;
		Map<String, Object> mapInfoGeneralArchivo = null;

		try {
			for (LayoutsInfoGeneralArchivo layoutsInfoGeneralArchivo : lecturaLayouts.getInstance()
					.lecturaLayoutGeneral()) {

				if (layoutsInfoGeneralArchivo.getBank().equals(datosBusquedaConsulta.get(Constantes.BANCO))) {
					String infoGeneralArchivo = mapper.writeValueAsString(layoutsInfoGeneralArchivo);

					mapInfoGeneralArchivo = mapper.readValue(infoGeneralArchivo,
							new TypeReference<Map<String, Object>>() {
							});
				}

			}

			Map<String, Object> layoutDefinicion = (Map<String, Object>) mapInfoGeneralArchivo
					.get(Constantes.DEFINICION);

			String layoutDefinicionString = mapper.writeValueAsString(layoutDefinicion);

			definicion = new Definition();
			definicion = mapper.readValue(layoutDefinicionString, Definition.class);

		} catch (IOException e) {

			throw new GenericException(Constantes.LOG_ERROR_MAPEO_DEFINICION, e);

		} catch (Exception e) {
			throw new GenericException(Constantes.LOG_ERROR_EN_CONSULTAR_LAYOUTS, e);
		}

		return definicion;
	}
}
