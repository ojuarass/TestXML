package com.mx.nova.repository;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.nova.exception.GenericException;

import com.mx.nova.modelo.LayoutsInfoGeneralArchivo;
import com.mx.nova.util.Collection;
import com.mx.nova.util.Maper;

public class LecturaLayouts {
	private static LecturaLayouts lecturaLayout;

	public static LecturaLayouts getInstance() {

		if (lecturaLayout == null) {
			lecturaLayout = new LecturaLayouts();
		}

		return lecturaLayout;
	}

	public LayoutsInfoGeneralArchivo[] lecturaLayoutGeneral() throws GenericException {

		String collection = Collection.COLECCION_MONGO_LAYOUTS.getPath();

		LayoutsInfoGeneralArchivo[] definicionarray = null;

		JSONParser parser = new JSONParser();
		URL fileLocation = getClass().getClassLoader().getResource(collection);
		Object file;

		try {
			file = parser.parse(new FileReader(fileLocation.getFile()));

			ObjectMapper mapper = Maper.INSTANCE.getObjectMapper();

			String definicionHeaders = mapper.writeValueAsString(file);

			definicionarray = mapper.readValue(definicionHeaders.toString(), LayoutsInfoGeneralArchivo[].class);

		} catch (IOException | ParseException e) {
			throw new GenericException("Error al obtener layouts", e);
		}

		return definicionarray;
	}
	
	
	
}
