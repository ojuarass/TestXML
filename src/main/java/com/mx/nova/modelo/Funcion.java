
package com.mx.nova.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "nombre",
    "args"
})
public class Funcion implements Serializable {

	private static final long serialVersionUID = 7581414008301291860L;

	@JsonProperty("nombre")
	private String nombre;
	@JsonProperty("args")
	private List<String> args = new ArrayList<String>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Funcion() {
	}

	/**
	 * @param nombre
	 * @param args
	 */
	public Funcion(String nombre, List<String> args) {
		super();
		this.nombre = nombre;
		this.args = args;
	}

	@JsonProperty("nombre")
	public String getNombre() {
		return nombre;
	}

	@JsonProperty("nombre")
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonProperty("args")
	public List<String> getArgs() {
		return args;
	}

	@JsonProperty("args")
	public void setArgs(List<String> args) {
		this.args = args;
	}

}
