
package com.mx.nova.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "fields" })
public class Line implements Serializable {

	private static final long serialVersionUID = 7314505469194662020L;

	@JsonProperty("fields")
	private List<Field> fields = new ArrayList<>();

	/**
	 * No args constructor for use in serialization
	 */
	public Line() {
	}

	/**
	 * @param fields
	 */
	public Line(List<Field> fields) {
		super();
		this.fields = fields;
	}

	@JsonProperty("fields")
	public List<Field> getFields() {
		return fields;
	}

	@JsonProperty("fields")
	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Line [fields=");
		builder.append(fields);
		builder.append("]");
		return builder.toString();
	}

}
