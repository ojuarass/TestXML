
package com.mx.nova.modelo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "bank", "image", "fileType", "delimeter", "version", "definition" })
public class LayoutsInfoGeneralArchivo {

	@JsonProperty("name")
	private String name;
	@JsonProperty("bank")
	private String bank;
	@JsonProperty("image")
	private String image;
	@JsonProperty("fileType")
	private String fileType;
	@JsonProperty("delimeter")
	private char delimeter;
	@JsonProperty("version")
	private String version;
	@JsonProperty("definition")
	private Definition definition;

	/**
	 * No args constructor for use in serialization
	 */
	public LayoutsInfoGeneralArchivo() {
	}

	/**
	 * @param name
	 * @param bank
	 * @param image
	 * @param fileType
	 * @param delimeter
	 * @param version
	 * @param definition
	 */
	public LayoutsInfoGeneralArchivo(String name, String bank, String image, String fileType, char delimeter,
			String version, Definition definition) {
		super();
		this.name = name;
		this.bank = bank;
		this.image = image;
		this.fileType = fileType;
		this.delimeter = delimeter;
		this.version = version;
		this.definition = definition;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("bank")
	public String getBank() {
		return bank;
	}

	@JsonProperty("bank")
	public void setBank(String bank) {
		this.bank = bank;
	}

	@JsonProperty("image")
	public String getImage() {
		return image;
	}

	@JsonProperty("image")
	public void setImage(String image) {
		this.image = image;
	}

	@JsonProperty("fileType")
	public String getFileType() {
		return fileType;
	}

	@JsonProperty("fileType")
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@JsonProperty("delimeter")
	public char getDelimeter() {
		return delimeter;
	}

	@JsonProperty("delimeter")
	public void setDelimeter(char delimeter) {
		this.delimeter = delimeter;
	}

	@JsonProperty("version")
	public String getVersion() {
		return version;
	}

	@JsonProperty("version")
	public void setVersion(String version) {
		this.version = version;
	}

	@JsonProperty("definition")
	public Definition getDefinition() {
		return definition;
	}

	@JsonProperty("definition")
	public void setDefinition(Definition definition) {
		this.definition = definition;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LayoutsInfoGeneralArchivo [name=");
		builder.append(name);
		builder.append(", bank=");
		builder.append(bank);
		builder.append(", image=");
		builder.append(image);
		builder.append(", fileType=");
		builder.append(fileType);
		builder.append(", delimeter=");
		builder.append(delimeter);
		builder.append(", version=");
		builder.append(version);
		builder.append(", definition=");
		builder.append(definition);
		builder.append("]");
		return builder.toString();
	}

}
